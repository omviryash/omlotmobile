
var LeftSideTime;
var RightSideTime;

var diffTime   = "";
var currenTime = "";
var flag     = true;
var ajaxFlag = true;
var allyantra = true;
var montharray;
var serverdate;


function clearElements(){
  $('#1').val('');
  $('#2').val('');
  $('#3').val('');
  $('#4').val('');
  $('#5').val('');
  $('#6').val('');
  $('#7').val('');
  $('#8').val('');
  $('#9').val('');
  $('#10').val('');
  $('#qty').val('');
  $('#amt').val('');
  $('#scancode').val('');
  $('.yantra_list li').each(function(index, element) {
    $(this).find('.form-group').hide(); 
  });
}

function customAlert($msg)
{
    swal({title: $msg,confirmButtonColor: "#F660AB"});
}

function padlength(what){
  var output=(what.toString().length==1)? "0"+what : what
  return output
}

// Execute when click on UpComing Button
function UpComing(){
  $.ajax({
    url:"ajax.php",
    data:"act=upcoming",
    type:"POST",
    datatype:"json",
    success:function(data){
      data = JSON.parse(data);
      /*var Ydate = new Date();
      Ytime = Math.floor(Ydate.getTime());
      JPDate = Ytime + (SLDiff * 1000);
            
      var date = new Date();
      date.setTime(JPDate)*/
      $("#centerPopupInner").html('');
      olddate = "";
      $.each(data, function(index, element) {
          currentTime = element.currentTime;
          i = currentTime.substring(0, 5);
          newdate = element.currentFullDateTime.substring(0, 10);
          if(olddate != newdate){
            $('#centerPopupInner').append('<div class="currenDate">'+ newdate.substring(3, 5) + "/" + newdate.substring(0, 2) + "/" + newdate.substring(6, 10) + '</div>');
          }
          $('#centerPopupInner').append('<span><input type="button" name="t'+i+'" value="'+element.currentTime+'" datacurrentFullDateTime="'+element.currentFullDateTime+'" datadrawid="'+element.draw_id+'" /></span>');
          olddate = newdate;
          //console.log(olddate);
      });
      
      $('#centerPopupInner input').addClass("centerPopupinput");
      var overlay = $("#centerPopup");
      top = $(window).height() - ($("#centerPopup").height() / 2);
      left = -(overlay.outerWidth() / 2);
      overlay.css({'margin-top': top,'margin-left': left+180});
      if(olddate != "") $('#centerPopup').show();
      
    },
    error:function(){
      //customAlert("Error while loading try again");
    }
  });
}

function displaytime(){
  serverdate.setSeconds(serverdate.getSeconds()+1)
  var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear()
  var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())
  //document.getElementById("currentTimer").innerHTML=datestring+" "+timestring
  document.getElementById("currentTimer").innerHTML=timestring
}

function update() {
  $('#ltime').text(secondsToTime(diffTime));
  diffTime    = diffTime - 1;
  currenTime  = parseInt(currenTime) + 1;
  if(diffTime==0 && ajaxFlag){
    //window.location.reload();
    ajaxFlag =  false;
    getCurrentData(0);
    allyantra = false;
    //getResult();
  } 
  if(flag){
    flag = false;
    LeftSideTime = setInterval(function(){update(); }, 1000);
    RightSideTime = setInterval("displaytime()", 1000)
  }
}

function secondsToTime(seconds){
  if(seconds<0)
  seconds=0;
  var sec_num = parseInt(seconds);
  var hours   = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);        
  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}
  var time    = hours+':'+minutes+':'+seconds;
  return time; 
}

function msToTime(duration) {
  var milliseconds = parseInt((duration%1000)/100)
      , seconds = parseInt((duration/1000)%60)
      , minutes = parseInt((duration/(1000*60))%60)
      , hours = parseInt((duration/(1000*60*60))%24);
  
  hours = (hours < 10) ? "0" + hours : hours;
  minutes = (minutes < 10) ? "0" + minutes : minutes;
  seconds = (seconds < 10) ? "0" + seconds : seconds;
  
  return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
}

// Get result data
function getResult(){
  var list='';
  if(!allyantra){
    //olddraw = $("#draw_id").val();
    olddraw = $("#nextdrawid").val();
    $.ajax({
      url:"ajax.php",
      data:"act=getdrawresult&draw_id="+olddraw,
      type:"POST",
      datatype:"json",
      success:function(data){
        data = JSON.parse(data);
        $.each(data, function(index, element) {
          imgNo = parseInt(element.win_product_id);
          if(imgNo > 0){
            if(imgNo < 10) newImgNo = "0"+imgNo; else newImgNo = imgNo;
          }else{
            newImgNo = "";
            if(element.is_last == 1) {
              console.log("Last draw winner not found. recall to fetch result after 2 second");
              setTimeout(function() {
                getResult();
              }, 2000);
              return;
            }
          }
          
          imgNo2 = parseInt(element.win_product_id2);
          if(imgNo2 > 0){
            if(imgNo2 < 10) newImgNo2 = "0"+imgNo2; else newImgNo2 = imgNo2;
          }else{
            newImgNo2 = "";
          }
          if(newImgNo2 !=''){
            if(element.is_jackpot == 'YES' && element.is_jackpot2 == 'YES'){
            list = list + '<div class="y-c-text t"><div class="sbgy l"></div>' + element.currentTime + '<div class="sbgy r"></div></div>';            
            }else if(element.is_jackpot == 'YES'){
              list = list + '<div class="y-c-text t"><div class="sbgy l"></div>' + element.currentTime + '</div>';
            }else if(element.is_jackpot2 == 'YES'){
              list = list + '<div class="y-c-text t">' + element.currentTime + '<div class="sbgy r"></div></div>';
            }else{
              list = list + '<div class="y-c-text t bgr">' + element.currentTime + '</div>';
            }
                        list = list + '<img class="y-c-img b" src="./images/'+imageLoad+'GW'+newImgNo+'.jpg" />';
                        list = list + '<img class="y-c-img b" src="./images/'+imageLoad+'GW'+newImgNo2 +'.jpg" />';
                      }else if(newImgNo !=''){
                      if(element.is_jackpot == 'YES'){
            list = list + '<div class="y-c-text l bgy">' + element.currentTime + '</div>';
                      }else{
            list = list + '<div class="y-c-text l bgr">' + element.currentTime + '</div>';
                      }
                        list = list + '<img class="y-c-img r" src="./images/'+imageLoad+'GW'+newImgNo+'.jpg" />';
                      }else{
                        list = list + '<div class="y-c-text l bgr">' + element.currentTime + '</div>';
                      }
                  $("#"+olddraw).html(list);
                  $('.yantra_content li').removeClass('lastYantraID');
                  $("#"+olddraw).attr('class', 'lastYantraID');
          parentDivTop = $('#content').scrollTop();
          innerContentHeight = $('#innerContent').innerHeight();
          lastYantraIDHeight = $('.lastYantraID').innerHeight();
          offsetval = $('.lastYantraID').position();
          lastYantraIDPosition = (offsetval.top - 20);
          $('#content').animate({"scrollTop": lastYantraIDPosition}, "slow");
          $("#content").animate({ scrollTop: $('.lastYantraID').offset().top - $(".lastYantraID").height() - 20 }, "slow");
          
        });updateUserBalance(olddraw);
        getBalance();
      },
      error:function(){
        //customAlert("Error while loading try again");
      }
    });
  }else{
    $.ajax({
      url:"ajax.php",
      data:"act=getresult&FullTime="+$("#ctime").html(),
      type:"POST",
      datatype:"json",
      success:function(data){
        
        data = JSON.parse(data);
        $('#content').html('<div id="innerContent"></div>');
        is_lastYantraID = 0;
        last_draw_id=0;
        rCount = 0;
        $.each(data, function(index, element) {
          rCount++;
          imgNo = parseInt(element.win_product_id);
          if(imgNo > 0){
            if(imgNo < 10) newImgNo = "0"+imgNo; else newImgNo = imgNo;
          }else{
            newImgNo = "";
          }
          
          imgNo2 = parseInt(element.win_product_id2);
          if(imgNo2 > 0){
            if(imgNo2 < 10) newImgNo2 = "0"+imgNo2; else newImgNo2 = imgNo2;
          }else{
            newImgNo2 = "";
          }
          
          if(element.is_last == '1'){
            newId = 'lastYantraID';
            is_lastYantraID++;
            last_draw_id=element.draw_id;
          }else{
            newId = '';
          }
          
          draw_id = element.draw_id;
          list = list + '<li class="'+ newId +'" id="'+draw_id+'">';
          if(newImgNo2 !=''){
            if(element.is_jackpot == 'YES' && element.is_jackpot2 == 'YES'){
              list = list + '<div class="y-c-text t"><div class="sbgy l"></div>' + element.currentTime + '<div class="sbgy r"></div></div>';            
            }else if(element.is_jackpot == 'YES'){
              list = list + '<div class="y-c-text t"><div class="sbgy l"></div>' + element.currentTime + '</div>';
            }else if(element.is_jackpot2 == 'YES'){
              list = list + '<div class="y-c-text t">' + element.currentTime + '<div class="sbgy r"></div></div>';
            }else{
              list = list + '<div class="y-c-text t bgr">' + element.currentTime + '</div>';
            }
                        list = list + '<img class="y-c-img b" src="./images/'+imageLoad+'GW'+newImgNo+'.jpg" />';
                        list = list + '<img class="y-c-img b" src="./images/'+imageLoad+'GW'+newImgNo2 +'.jpg" />';
                      }else if(newImgNo !=''){
                      if(element.is_jackpot == 'YES'){
            list = list + '<div class="y-c-text l bgy">' + element.currentTime + '</div>';
                      }else{
            list = list + '<div class="y-c-text l bgr">' + element.currentTime + '</div>';
                      }
                        list = list + '<img class="y-c-img r" src="./images/'+imageLoad+'GW'+newImgNo+'.jpg" />';
                      }else{
                        list = list + '<div class="y-c-text l bgr">' + element.currentTime + '</div>';
                      }
          list = list + '</li>';
          if(rCount%2==0)
          list = list + '<div class="clearfix"></div>';
        });
            newTable = '<ul class="yantra_content clearfix">'+list+'</ul>';
            jQuery("#innerContent").html(newTable);
            $('#content').scrollTop(0);
        if(is_lastYantraID > 0){
          parentDivTop = $('#content').scrollTop();
          innerContentHeight = $('#innerContent').innerHeight();
          lastYantraIDHeight = $('.lastYantraID').innerHeight();
          offsetval = $('.lastYantraID').position();
          lastYantraIDPosition = (offsetval.top - 20);
          $('#content').animate({"scrollTop": lastYantraIDPosition}, "slow");
            }
          
      },
      error:function(){
        //customAlert("Error while loading try again");
      }
    });
  }
  
}

// Get company balance
function getBalance(){
  
  $.ajax({
    url:"ajax.php",
    data:"act=getBalance",
    type:"POST",
    datatype:"json",
    success:function(data) {
      if(data != '') {
        $('.currentBalance').html(data);
      }
    },
    error:function(){
      //customAlert("Error while loading try again");
    }
  });
}

// Execute when click on Current Button
function getCurrentData(isupcomming){
  clearInterval(LeftSideTime);
  clearInterval(RightSideTime);
  ajaxFlag = true;
  flag = true;
  
  if(isupcomming == 0)
    qStr = "act=loadData";
  else
    qStr = "act=loadData&drawid="+isupcomming;
  $.ajax({
    url:"ajax.php",
    data:qStr,
    type:"POST",
    datatype:"json",
    success:function(data){
    //customAlert("====="+data);
    //return false;
    if(data){
      data = JSON.parse(data);
      //customAlert(data[3]);
      if(data[3] != ""){
        $("#draw_id").val(data[3]);
        $("#currentFullDateTime").html(data[2]);
        $("#ctime").html(data[1]);
        //customAlert(data[1]);
        //clearInterval(LeftSideTime);
        //LeftSideTime = setInterval(startLeftTime,1000);
        
        diffTime     = data[0];
        currenTime   = data[2];
        montharray   = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
        serverdate   = new Date(currenTime);
        update();
        //$('#nextDrawTime').text(data[1]);
        
      }
    }else{
      $("#ctime").html('');
      $("#ltime").html('');
      $("#currentFullDateTime").html('');
      clearInterval(LeftSideTime);
      clearInterval(RightSideTime);
    }
    },
    error:function(){
      
      //customAlert("Error while loading try again");
    }
  });
}

//Update user Balance After Draw
function updateUserBalance(drawid){
  
  $.ajax({
    url:"ajax.php",
    data:"act=updateUserBalance&drawid="+drawid,
    type:"POST",
    async: false,
    success:function(data) {
      /*if(data != '') {
        $('.currentBalance').html(data);
      }*/
    },
    error:function(){
      //customAlert("Error while loading try again");
    }
  });
}

// set Win Product
function setWinProduct(){
  $.ajax({
    url:"ajax.php",
    data:"act=setWinProduct",
    type:"POST",
    datatype:"json",
    success:function(data){},
    error:function(){}
  });
}

function updatecurrenttime(){
  allyantra=true;
  if($('#currenttime').val()!=""){
  //console.log($('#currenttime').val());
  $('#currenttime').val(parseInt($('#currenttime').val())+1);
  
  if($('#currenttime').val()==$('#nextdrawtime').val()){
    allyantra=false;
    getResult();
    getNextDraw();
  }
  }
}

function getNextDraw(){
  
  $.ajax({
    url:"ajax.php",
    data:"act=getNextDraw",
    type:"POST",
    datatype:"json",
    //async:false,
    success:function(data) {
      data=JSON.parse(data);
      //alert(data["draw_id"]);
      if(data['eror']=='0') {
        $('#nextdrawid').val(data['draw_id']);
        $('#nextdrawtime').val(data['nextdrawtime']);
        $('#currenttime').val(data['currenttime']);
      }
    },
    error:function(){
      //customAlert("Error while loading try again");
    }
  });
}

function calQtyAmt(){
  product_price = $("#product_price").val();
  $("#qty").val('');
  $("#amt").val('');
  qty = 0;
  $('.udlrClass').each(function(){
    if(parseInt($(this).val()) > 0)
      qty = qty + parseInt($(this).val());
  });
  if(qty > 0){
    $("#qty").val(qty);
    $("#amt").val((qty * product_price));
  }
}

$(document).ready(function() {
  
  //loop tab index for yantra textbox
  $('#10').on('keydown', function (e){
      var keyCode = e.keyCode || e.which; 
      if (keyCode == 9 && !e.shiftKey) { 
        e.preventDefault(); 
        $('#1').focus();
      } 
  });
  
  $('#receipt').on('keypress', function (e) {
    if (e.which == 13) {
      barcode = 0;
      if($('#1').val() > 9999) barcode = $('#1').val();
      if($('#2').val() > 9999) barcode = $('#2').val();
      if($('#3').val() > 9999) barcode = $('#3').val();
      if($('#4').val() > 9999) barcode = $('#4').val();
      if($('#5').val() > 9999) barcode = $('#5').val();
      if($('#6').val() > 9999) barcode = $('#6').val();
      if($('#7').val() > 9999) barcode = $('#7').val();
      if($('#8').val() > 9999) barcode = $('#8').val();
      if($('#9').val() > 9999) barcode = $('#9').val();
      if($('#10').val() > 9999) barcode = $('#10').val();
      if(barcode > 9999){
        clearElements();
        $('#scancode').val(barcode);
        $('#loadWinner').submit();
      }
      return false;
    }
  });
  
  $('#receipt').on('submit', function (e) {
    e.preventDefault();
    barcode = 0;
    if($('#1').val() > 9999) barcode = $('#1').val();
    if($('#2').val() > 9999) barcode = $('#2').val();
    if($('#3').val() > 9999) barcode = $('#3').val();
    if($('#4').val() > 9999) barcode = $('#4').val();
    if($('#5').val() > 9999) barcode = $('#5').val();
    if($('#6').val() > 9999) barcode = $('#6').val();
    if($('#7').val() > 9999) barcode = $('#7').val();
    if($('#8').val() > 9999) barcode = $('#8').val();
    if($('#9').val() > 9999) barcode = $('#9').val();
    if($('#10').val() > 9999) barcode = $('#10').val();

    if(barcode > 9999){
      clearElements();
      $('#scancode').val(barcode);
      $('#loadWinner').submit();
    }else{
      if(parseFloat($('.currentBalance').html()) > 0){
        if($('#draw_id').val() > 0){
          if($('#1').val() > 0 || $('#2').val() > 0 || $('#3').val() > 0 || $('#4').val() > 0 || $('#5').val() > 0 || $('#6').val() > 0 || $('#7').val() > 0 || $('#8').val() > 0 || $('#9').val() > 0 || $('#10').val()){
            tmpdata = $('#receipt').serialize();
            clearElements();
            $.ajax({
              type: 'post',
              url: 'mpost.php',
              data: tmpdata,
              complete:(function(data) {
                if(data.responseText == "-1"){
                  customAlert("Draw is already completed.");
                  window.location.reload();
                }else if(data.responseText == "0"){
                  customAlert("No Balance");
                }
                getBalance();
              })
            });     
          }else{
            customAlert("Please enter quantity.");
          }
        }else{
          customAlert("Draw is not available.");
        }
      }else{
        customAlert("No Balance");
      }
    }
  });
  
  $('#buy').on('click', function () {
    $('#receipt').submit();
  });

  $(document).on( 'keydown', function ( e ) {
    //console.log("asd"+e.keyCode);
    if ( e.keyCode === 27 ) { // ESC
       if($("#centerPopup").is(':visible')){
       $("#centerPopup").hide();
       getCurrentData(0);
       }
    } else if ( e.keyCode === 120 ) { // F9
       $('#can').click();
    } else if ( e.keyCode === 117 ) { // F6
              e.preventDefault();
       $('#receipt').submit();
    } else if ( e.keyCode === 119 ) { // F8
       $('#scancode').focus();
    } else if ( e.keyCode === 116 ) { // F5
             e.preventDefault(); 
       clearElements();
    } else if ( e.keyCode === 118 ) { // F7
       window.open('luckydraw.php', '_blank');
    } else {
      // 
    }
  });
  
  $('body').on('click', '#closeDiv', function() {
      $("#centerPopup").hide();
      getCurrentData(0);
  });
  
  $('#CurrentTime, #slider1_container, #Righttop, #yantra_content').on('click', function (e) {
    $("#1").focus();
  });

  $("#1").focus();

  $(".onlynum_old").keydown(function (e) {

      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
          (e.keyCode == 65 && e.ctrlKey === true) || 
          (e.keyCode >= 35 && e.keyCode <= 39)) {
          return;
      }
      //if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    if ((e.shiftKey) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });
  $(".onlynum").keyup(function (e) {
    if($(this).val().length >= 13)
    {
      $('#scancode').val($(this).val());
      $(this).val('');
      $('#loadWinner').submit();
    }
  });
  $('.yantra_list li').mouseover(function(){
      $(this).find('.form-group').show();
  })
  $('.yantra_list li').mouseleave(function(){
      if($(this).find('.form-group').find('.onlynum').val().length ==0){
          $(this).find('.form-group').hide();
      }
  })

  $('body').on('click', '.centerPopupinput', function() {
    $("#centerPopup").hide();
    clearInterval(LeftSideTime);
    clearInterval(RightSideTime);
    
    ajaxFlag = true;
    flag = true;
    getCurrentData($(this).attr("datadrawid"));
  });

  // Last Receipt
  $('body').on('click', '.lreceipt', function() { 
    $("#iframeDiv").html('');
    $('#iframeDiv').append("<iframe src='ticket.php?lastrec=true'></iframe>");
  });

  // Purchase Receipt
  $('body').on('click', '.pdetail', function() {  
    window.open('mpurchase.php', '_self');
  });

  // Lucky Draw
  $('body').on('click', '.luckyyantra', function() {  
    window.open('luckydraw.php?ref=mobile', '_self');
  });
  
  // Draw List
  $('body').on('click', '.drawlist', function() { 
    window.open('drawlist.php', '_blank');
  });

  // Logout
  $('body').on('click', '.exit', function() { 
    if(cpackage == 3){
      swal({
        title: "Are you sure, you want to logout?",
        showCancelButton: true,
        cancelButtonColor: "#ffff00",
        confirmButtonColor: "#F660AB",
        confirmButtonText: "OK",
        closeOnConfirm: true
      },
      function(){  
        window.location.replace('logout.php');
      });
    }
  });

  getCurrentData(0);

  getResult();

  $('body').on('keyup', '.udlrClass', function(e) {
    calQtyAmt();
    var thisIndex = parseInt($(this).attr("dataIndex"));
    var thisId = $(this).attr("id");
    var newIndex = null;
    if (e.keyCode == 37) {  //left
      if(thisIndex == 1)
        $("#10").focus().select();
      else
        $("#"+(thisIndex-1)).focus().select();
    }
    if (e.keyCode == 38) {  //up
      if(thisIndex == 6) $("#1").focus().select();
      if(thisIndex == 7) $("#2").focus().select();
      if(thisIndex == 8) $("#3").focus().select();
      if(thisIndex == 9) $("#4").focus().select();
      if(thisIndex == 10) $("#5").focus().select();
    }
    if (e.keyCode == 39) {  //right
      if(thisIndex == 10)
        $("#1").focus().select();
      else
        $("#"+(thisIndex+1)).focus().select();
    }
    if (e.keyCode == 40) {  //down
      if(thisIndex == 1) $("#6").focus().select();
      if(thisIndex == 2) $("#7").focus().select();
      if(thisIndex == 3) $("#8").focus().select();
      if(thisIndex == 4) $("#9").focus().select();
      if(thisIndex == 5) $("#10").focus().select();
    }
  });

  $('#loadWinner').on('submit', function (e) {
    e.preventDefault();
    if($('#scancode').val().length > 3){    
      scancode = $('#scancode').val();
      $.ajax({
        url:"ajax.php",
        data:"act=getwinner&hash_key="+scancode,
        type:"POST",
        success:function(data){
          var data = data.split(",");
          if(data[0] == 'winner'){
            swal({
              title: "You won silver coin(s) worth Rs. " + data[1] + ". Confirm to proceed further?",
              showCancelButton: true,
              cancelButtonColor: "#ffff00",
              confirmButtonColor: "#F660AB",
              confirmButtonText: "OK",
              closeOnConfirm: true
            },
            function(){
              $.ajax({
                url:"ajax.php",
                data:"act=getwinnerupdate&hash_key="+scancode,
                type:"POST",
                success:function(data){
                  //customAlert("updated")
                }
              });

              $("#iframeDiv").html('');
              $('#iframeDiv').append("<iframe src='winner.php?id="+scancode+"'></iframe>");
              getBalance();
            });
          }else if(data[0] == 'cancel'){
            customAlert("Sorry, Your receipt was cancel.");
          }else if(data[0] == 'scan'){
            customAlert("Sorry, Your receipt was already scan at "+data[1]);
          }else if(data[0] == 'nowinner'){
            customAlert("Sorry, Your are not winner.");
          }else if(data[0] == 'pending'){
            customAlert("Sorry, Draw is still pending.");
          }else if(data[0] == 'noretailer'){
            customAlert("Sorry, Receipt is not available with this retailer.");
          }else if(data[0] == 'nodraw'){
            customAlert("Sorry, Draw is not available.");
          }else{
            customAlert("Sorry, Bar code incorrect.");
          }
          $('#scancode').val('');
        },
        error:function(){
          //customAlert("Error while loading try again");
          $('#scancode').val('');
        }
      });
    }else{
      customAlert("Sorry, Bar code length incorrect.");
                  $('#scancode').val('');
    }
    
  });

  $('#can').on('click', function (e) {
    e.preventDefault();
    swal({
      title: "Are you sure to cancle receipt?",
      showCancelButton: true,
      cancelButtonColor: "#ffff00",
      confirmButtonColor: "#000080",
      confirmButtonText: "OK",
      closeOnConfirm: true
    },
    function(){
      $.ajax({
        url:"ajax.php",
        data:"act=canReceipt",
        type:"POST",
        success:function(data){
          if(data == '-1') {
            customAlert("Your cancel receipt limit over.");
          } else if(data == '1') {
            getBalance();
            customAlert("Last receipt is cancel.");
          } else
            customAlert("Sorry, Draw is already done and we can't cancel last receipt.");
        },
        error:function(){
                //customAlert("Error while loading try again");
        }
      });   
    }); 
  });

  $('#clear').on('click', function (e) {
    e.preventDefault();
    clearElements();
  });

  //scrollto last result
  $('#lastresult').click(function() {
    $('html, body').animate({
      scrollTop: $("#content .lastYantraID").offset().top
    }, 2000);
    return false;
  });

  getNextDraw();

  setInterval(function(){updatecurrenttime();},1000);    

  $('#scancode').on('keyup', function (e) {
    if($(this).val().length >= 13)
      $('#loadWinner').submit();
  });
});
