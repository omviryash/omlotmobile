<?php
include_once('includes/basepath.php');
	function get_data($data){
		$users = array();
		$usr_qry = "SELECT u.user_id,u.username,rm.user_commission, rm.draw_id, rm.receipt_id FROM users as u LEFT JOIN receipt_master as rm ON u.user_id = rm.retailer_id WHERE is_active = '1'";
		if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != "")
		{
			$usr_qry .= " AND u.user_id = {$_SESSION['user_id']}";
		}
		$usr_qry .= " ORDER BY u.username";
		$usr_qry_rs = mysql_query($usr_qry);
		if(mysql_num_rows($usr_qry_rs) > 0){
			$key = 0;
			while($usr_row = mysql_fetch_array($usr_qry_rs)){
				$transaction = array();
				$credit = get_transaction($usr_row['user_id'], $data['from_date'], $data['to_date'],'Credit');
				$debit = get_transaction($usr_row['user_id'], $data['from_date'], $data['to_date'],'Debit');
				if(!empty($credit))
				{
					foreach($credit as $key => $value){
						$transaction[] = $value;
					}
				}
				if(!empty($debit))
				{
					foreach($debit as $key => $value){
						$transaction[] = $value;
					}
				}
				usort($transaction,"array_sort");
				$users[$key] = $usr_row;
				$users[$key]['transaction'] = $transaction;
				$key++;
			}
		}
		return $users;
	}
	
	function get_transaction($retailer_id,$fromdate,$todate,$flage)
	{
		$trans = array();
		$trns_qry = "SELECT *, COUNT(amount) as {$flage} FROM transaction WHERE cr_dr = '{$flage}' AND retailer_id = {$retailer_id} AND (DATE(transaction_time) BETWEEN '{$fromdate}' AND '{$todate}') GROUP BY DATE(transaction_time) ORDER BY transaction_time";
		$trns_qry_rs = mysql_query($trns_qry);
		if(mysql_num_rows($trns_qry_rs) > 0){
			$key = 0;
			while($trns_row = mysql_fetch_array($trns_qry_rs)){
				$trans[$key]['cr_dr'] = $trns_row['cr_dr'];
				$trans[$key]['amount'] = $trns_row['amount'];
				$trans[$key]['transaction_time'] = date("Y-m-d",strtotime($trns_row['transaction_time']));
				$key++;
			}
		}
		return $trans;
	}
	
	function array_sort($a,$b)
	{
		if ($a['transaction_time'] == $b['transaction_time']) {
	        return 0;
	    }
	    return ($a['transaction_time'] < $b['transaction_time']) ? -1 : 1;
	}

	/* Coding Start here */
	
if(!isset($_SESSION['user_id'])){
	header('Location: ./index.php');
	exit;
}

$toDate = isset($_POST['to_date']) ? substr($_POST['to_date'],0,4)."-".substr($_POST['to_date'],5,2)."-".substr($_POST['to_date'],8,2) : date("Y-m-d");
$forDate = isset($_POST['from_date']) ? substr($_POST['from_date'],0,4)."-".substr($_POST['from_date'],5,2)."-".substr($_POST['from_date'],8,2) : date("Y-m-d");

$result = array();
if(isset($_POST) && !empty($_POST)) {
	$post_data = $_POST;
	$result = get_data($post_data);
} 
?>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="js/plugins/sweetalert/sweet-alert.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/bootstrapreset.css"/>
	<link rel="stylesheet" href="css/mobile.css"/>

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert/sweet-alert.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" /> 
	<script type="text/javascript" src="js/jquery-ui.js"></script>
	<script language="javascript">
    jQuery(document).ready(function() {
        jQuery('#from_date').datepicker({ dateFormat: 'yy-mm-dd' });
        jQuery('#to_date').datepicker({ dateFormat: 'yy-mm-dd' });
        //.datepicker("setDate", new Date())
    })
  </script>

	</head>
<body>
	<div id="centerPopup">
      <div class="closeDiv">
          <input type="button" value="X" class="close" id="closeDiv" />
      </div>
      <div id="centerPopupInner"></div>
    </div>
    <header>
        <div class="container">
            <div class="row text-center">
                <div class="col-md-4 col-xs-4 col-sm-4">
                    <div class="time-info" id="CurrentTime">
                        <p id="ctime">7:09</p>
                        <p id="ltime"></p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 col-sm-4">
				    <div class="prcblkinfo">
                        <p class="currentBalance">
                            <?php
							$sSQL = "SELECT current_balance FROM users WHERE user_id = ".$_SESSION['user_id'];
							$rs = mysql_query($sSQL) or print(mysql_error());
							$row = mysql_fetch_array($rs);
							echo round($row["current_balance"]);
							?>
                        </p>
                        <p class="pnno"><?php echo rtnretailer($_SESSION['user_id']); ?></p>                            
				    </div>                    
                </div>
                <div class="col-md-4 col-xs-4 col-sm-4">
				    <p id="currentTimer" class="curTime">02:14:12</p>
				    <p class="dtcls"><?php echo date('d/m/Y');?></p>                        
                </div>
            </div>
        </div>
    </header>
    <section class="contants">
	<div class="container">
            <div class="row">
                <div class="col-md-12">
					<div id="yantra_list" class="clearfix">
						<form name="frm" action="mbalance_type.php?btype=<?php echo $_REQUEST["btype"]; ?>" method="post">
							<input type="hidden" name="btype" id="btype" value="<?php echo $_REQUEST["btype"]; ?>" />
							<table width="100%" cellspacing="0" cellpadding="0" class="table table-responsive">
								<tr>
									<td>From Date</td>
									<td><input type="text" id="from_date" name="from_date" placeholder="Select Date" value="<?php  echo $forDate;  ?>"></td>
								</tr>
								<tr>
									<td>To Date</td>
									<td><input type="text" id="to_date" name="to_date" placeholder="Select Date" value="<?php  echo $toDate;  ?>"></td>
								</tr>
								<tr>
									<td>Summary</td>
									<td><select name="cmbSummary">
										<option value="full"<?php echo (isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='full')?" selected=\"selected\"":""; ?>>Full Summary</option>
										<option value="half"<?php echo (isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='half')?" selected=\"selected\"":""; ?>>Half Summary</option>
										<option value="detail"<?php echo (isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='detail')?" selected=\"selected\"":""; ?>>Detail Summary</option>
									</select>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center">
										<input type="submit" name="submitBtn" class="btn btn-primary" value="Go">
										<input class="btn btn-primary" type="button" name="back" value="Back" id="back">
										
									</td>
								</tr>
							</table>
						</form>	
						<div id="mainWrapper" style="margin-top:20px;">
							<div class="box-body table-responsive">
								<?php
								if(isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='full') {
									include_once("mbalance_type_full_summary.php"); 
								}
								if(isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='half') {
									include_once("mbalance_type_half_summary.php"); 
								}
								if(isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='detail') {
									include_once("mbalance_type_detail_summary.php"); 
								}
								?>
							</div><!-- /.box-body -->
						</div> 
			<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery('#back').on('click', function() {
					window.location.replace('mdashboard.php');
				});
			});

			</script>
</body>
</html>

